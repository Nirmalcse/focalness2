#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define sizex 100
#define sizey 100
#define sizez 100
#define ipimage "FDT_downsampled_hs_ellipse_11x11x11.img"
#define opimage "focalness_FDT_downsampled_hs_ellipse_11x11x11.img"
#define lo 1
#define d1 3
#define d2 4
#define d3 5
#define ws 1

int npp[27][3]={
                {-1,-1,-1},{0,-1,-1},{1,-1,-1},
                {-1, 0, -1},{0,0,-1},{1,0,-1},
                {-1, 1, -1},{0,1,-1},{1,1,-1},

                {-1,-1,0},{0,-1,0},{1,-1,0},
                {-1, 0,0},{0,0,0},{1,0,0},
                {-1, 1,0},{0,1,0},{1,1,0},

                {-1, -1, 1},{0,-1,1},{1,-1,1},
                {-1, 0, 1},{0,0,1},{1,0,1},
                {-1, 1, 1},{0,1,1},{1,1,1},
               };
unsigned short curlabel=1,compsize[29];
int neighbor[27]={d3,d2,d3,
                  d2,d1,d2,
                  d3,d2,d3,

                  d2,d1,d2,
                  d1,0,d1,
                  d2,d1,d2,

                  d3,d2,d3,
                  d2,d1,d2,
                  d3,d2,d3};
int sixnpp[6][3]={
                    {0,0,-1},

                    {0,-1,0},
           {-1,0,0},        {1,0,0} ,
                    {0,1,0},

                    {0,0,1}

};
signed short gx[27]={-1,-3,-1,-3,-6,-3,-1,-3,-1,
                      0,0,0,0,0,0,0,0,0,
                      1,3,1,3,6,3,1,3,1
       };
signed short gy[27]={1,3,1,0,0,0,-1,-3,-1,
                     3,6,3,0,0,0,-3,-6,-3,
                     1,3,1,0,0,0,-1,-3,-1
             };
signed short gz[27]={-1,0,1,-3,0,3,-1,0,1,
                     -3,0,3,-6,0,6,-3,0,3,
                     -1,0,1,-3,0,3,-1,0,1
             };
signed short gxp[27]={-1,-1,-1, -1,-1,-1, -1,-1,-1,
                       0,0,0,     0,0,0,    0,0,0,
                       1,1,1,     1,1,1,    1,1,1
       };
signed short gyp[27]={1,1,1, 0,0,0, -1,-1,-1,
                      1,1,1, 0,0,0, -1,-1,-1,
                      1,1,1, 0,0,0, -1,-1,-1
             };
signed short gzp[27]={-1,0,1, -1,0,1, -1,0,1,
                      -1,0,1, -1,0,1, -1,0,1,
                      -1,0,1, -1,0,1, -1,0,1
             };
unsigned short angle[27]={8,7,8,
                          7,10,7,
                          8,7,8,

                         7,10,7,
                         10,10,10,
                         7,10,7,

                           8,7,8,
                           7,10,7,
                           8,7,8
                     };
signed short grady[sizex][sizey][sizez],gradx[sizex][sizey][sizez],gradz[sizex][sizey][sizez];
unsigned short gradyp[sizex][sizey][sizez],gradxp[sizex][sizey][sizez],gradzp[sizex][sizey][sizez];
unsigned short sobelgrad[sizex][sizey][sizez],a[sizex][sizey][sizez],pixel,dtv[sizex][sizey][sizez],temp[3][3][3],focalness1[sizex][sizey][sizez];
unsigned int focalness[sizex][sizey][sizez],prewittgrad[sizex][sizey][sizez];
void dfs(int,int,int);
void draw(unsigned short[sizex][sizey][sizez]);
void dt();
void ldt();
void maxsixcomponent();
void calculate_gradx();
void calculate_grady();
void calculate_gradz();
void calculate_gradxp();
void calculate_gradyp();
void calculate_gradzp();
void sobel();
void prewitt();
void normalization(unsigned int[sizex][sizey][sizez]);
int main(void)
{
  FILE *fp;
  int i,j,k;
  int pixel1;
  fp=fopen(ipimage,"rb");
  if(!fp)
  {
      printf("\nUnable to open file");
      exit(0);
  }
  for(k=0;k<sizez;k++)        // read the image
  {
      for(j=0;j<sizey;j++)
      {
          for(i=0;i<sizex;i++)
          {
              if(fread(&pixel,sizeof(unsigned short),1,fp)==1)
              {
               a[i][j][k]=(unsigned short)pixel;
               focalness[i][j][k]=0;
              }
          }
      }
  }
  fclose(fp);
  calculate_gradx();
  calculate_grady();
  calculate_gradz();
  sobel();
  calculate_gradxp();
  calculate_gradyp();
  calculate_gradzp();
  prewitt();
  ldt();
  draw(focalness1);
  printf("\nDone!!");
  return 0;
}

void draw(unsigned short img[sizex][sizey][sizez])
{
    FILE *ft;
    int i,j,k,pixel2;
    ft=fopen(opimage,"wb");
    if(ft==NULL)
    {
        printf("\n Unable to open o/p file");
        exit(0);
    }

   for(k=0;k<sizez;k++)
   {
    for(j=0;j<sizey;j++)
    {
     for(i=0;i<sizex;i++)
     {
        pixel=img[i][j][k];
        fwrite(&pixel,sizeof(unsigned short),1,ft);
     }
    }
   }
fclose(ft);
}

void ldt()
{
    int  n,p,q,r,i,j,k,s=0,t,v,maxcomp,lc,cnt=0;
    unsigned int sum,maxf,min;
    for(k=0;k<sizez;k++)        // read the image
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                if(a[i][j][k]!=0)
                {
                    cnt=0;
                    /*s=0;*/
                    sum=0;
                    min=a[i][j][k];
                    for(r=0;r<3;r++)
                    {
                        for(q=0;q<3;q++)
                        {
                            for(p=0;p<3;p++)
                            {
                                temp[p][q][r]=0;
                            }
                        }
                    }
                    n=0;
                    for(v=0;v<3;v++)
                    {
                        for(t=0;t<3;t++)
                        {
                            for(s=0;s<3;s++)
                            {
                                    p=i+npp[n][0];
                                    q=j+npp[n][1];
                                    r=k+npp[n][2];
                                    n++;
                                    if(p==i&&q==j&&r==k);
                                    else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                                    {
                                        if(a[p][q][r]!=0)
                                        {
                                            if(a[p][q][r]<min)
                                                temp[s][t][v]=lo;
                                        }
                                    }
                            }
                        }
                    }
                    maxsixcomponent();
                    lc=2;
                    maxcomp=compsize[2];
                    for(n=2;n<29;n++)
                    {
                        if(compsize[n]!=0)
                        {
                            if(compsize[n]>maxcomp)
                            {
                                maxcomp=compsize[n];
                                lc=n;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    maxf=0;
                        n=0;
                        sum=0;
                        for(v=0;v<3;v++)
                        {
                           for(t=0;t<3;t++)
                           {
                              for(s=0;s<3;s++)
                              {
                                     if(temp[s][t][v]==lc)
                                     {
                                         sum=sum+prewittgrad[i][j][k]*angle[n];
                                         if(sum>USHRT_MAX)printf("Problematic");
                                         cnt++;
                                     }
                                  n++;
                               }
                            }
                        }
                        if(sum>maxf)
                        {
                            maxf=sum;
                            focalness[i][j][k]=maxf;
                        }


                  /* for(n=0;n<27;n++)
                    {
                        p=i+npp[n][0];
                        q=j+npp[n][1];
                        r=k+npp[n][2];
                        if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                        {
                            if(a[p][q][r]<a[i][j][k])
                            {
                                s=s+angle[n];
                                cnt++;
                            }
                        }
                    }
                    prewittgrad[i][j][k]=s;*/
                }
            }
        }
    }

   normalization(focalness);

}

void dfs(int x, int y, int z)
{
    int p,q,r,n;
    if(temp[x][y][z]>1)return;
    else
    {
         temp[x][y][z]=curlabel;
         compsize[curlabel]=compsize[curlabel]+1;
         for(n=0;n<6;n++)
         {
               p=x+sixnpp[n][0];
               q=y+sixnpp[n][1];
               r=z+sixnpp[n][2];
//             p=x+npp[n][0];
//             q=y+npp[n][1];
//             r=z+npp[n][2];
               if(p==x&&q==y&&z==r)continue;
               if(p>=0&&p<3&&q>=0&&q<3&&r>=0&&r<3&&temp[p][q][r]!=0)
               {
                   dfs(p,q,r);
               }
         }
    }
}

void maxsixcomponent()
{
  int i,j,k;
  for(k=0;k<29;k++)
  {
      compsize[k]=0;
  }
  curlabel=1;
  for(k=0;k<3;k++)
  {
      for(j=0;j<3;j++)
      {
          for(i=0;i<3;i++)
          {
              if(temp[i][j][k]!=0)
              {
                  curlabel++;
                  dfs(i,j,k);
              }
          }
      }
  }
}

void calculate_gradx()
{
 int i,j,k,p,q,r,n;signed short sum=0;
 for(k=0;k<sizez;k++)
 {
     for(j=0;j<sizey;j++)
     {
         for(i=0;i<sizex;i++)
         {
             sum=0;
             if(a[i][j][k]!=0)
             {
                 for(n=0;n<27;n++)
                 {
                     p=i+npp[n][0];
                     q=j+npp[n][1];
                     r=k+npp[n][2];
                     if(p==i&&q==j&&r==k);
                     else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                     {
                         sum=sum+(signed short)(a[p][q][r]*gx[n]);
                     }

                 }
             }
             gradx[i][j][k]=abs(sum);
         }
     }
 }
}

void calculate_grady()
{
    int i,j,k,p,q,r,n; signed short sum=0;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                 sum=0;
                if(a[i][j][k]!=0)
                {
                    for(n=0;n<27;n++)
                    {
                        p=i+npp[n][0];
                        q=j+npp[n][1];
                        r=k+npp[n][2];
                        if(p==i&&q==j&&r==k);
                        else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                        {
                            sum=sum+(signed short)(a[p][q][r]*gy[n]);
                        }
                    }
                }
                grady[i][j][k]=abs(sum);
            }
        }
    }
}

void calculate_gradz()
{
    int i,j,k,p,q,r,n; signed short sum=0;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                 sum=0;
                if(a[i][j][k]!=0)
                {
                    for(n=0;n<27;n++)
                    {
                        p=i+npp[n][0];
                        q=j+npp[n][1];
                        r=k+npp[n][2];
                        if(p==i&&q==j&&r==k);
                        else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                        {
                            sum=sum+(signed short)(a[p][q][r]*gz[n]);
                        }
                    }
                }
                gradz[i][j][k]=abs(sum);
            }
        }
    }
}

void calculate_gradxp()
{
    int i,j,k,p,q,r,n; signed short sum=0;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                 sum=0;
                if(a[i][j][k]!=0)
                {
                    for(n=0;n<27;n++)
                    {
                        p=i+npp[n][0];
                        q=j+npp[n][1];
                        r=k+npp[n][2];
                        if(p==i&&q==j&&r==k);
                        else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                        {
                            sum=sum+(signed short)(a[p][q][r]*gxp[n]);
                        }
                    }
                }
                gradxp[i][j][k]=abs(sum);
            }
        }
    }
}

void calculate_gradyp()
{
    int i,j,k,p,q,r,n; signed short sum=0;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                 sum=0;
                if(a[i][j][k]!=0)
                {
                    for(n=0;n<27;n++)
                    {
                        p=i+npp[n][0];
                        q=j+npp[n][1];
                        r=k+npp[n][2];
                        if(p==i&&q==j&&r==k);
                        else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                        {
                            sum=sum+(signed short)(a[p][q][r]*gyp[n]);
                        }
                    }
                }
                gradyp[i][j][k]=abs(sum);
            }
        }
    }
}

void calculate_gradzp()
{
    int i,j,k,p,q,r,n; signed short sum=0;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                 sum=0;
                if(a[i][j][k]!=0)
                {
                    for(n=0;n<27;n++)
                    {
                        p=i+npp[n][0];
                        q=j+npp[n][1];
                        r=k+npp[n][2];
                        if(p==i&&q==j&&r==k);
                        else if(p>=0&&p<sizex&&q>=0&&q<sizey&&r>=0&&r<sizez)
                        {
                            sum=sum+(signed short)(a[p][q][r]*gzp[n]);
                        }
                    }
                }
                gradzp[i][j][k]=abs(sum);
            }
        }
    }
}

/*void sobel()
{
  int i,j,k,max=0;
  for(k=0;k<sizez;k++)
  {
      for(j=0;j<sizey;j++)
      {
          for(i=0;i<sizex;i++)
          {
              sobelgrad[i][j][k]=(unsigned short)(gradx[i][j][k]+grady[i][j][k]+gradz[i][j][k]);
              //if(sobelgrad[i][j][k]>max)max=sobelgrad[i][j][k];
          }
      }
  }
  normalization(sobelgrad);
  for(k=0;k<sizez;k++)
  {
      for(j=0;j<sizey;j++)
      {
          for(i=0;i<sizex;i++)
          {
              if(a[i][j][k]!=0)
              sobelgrad[i][j][k]=255-sobelgrad[i][j][k];
          }
      }
  }
}*/

void prewitt()
{
    int i,j,k;
    unsigned int max;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                prewittgrad[i][j][k]=(unsigned int)(gradxp[i][j][k]+gradyp[i][j][k]+gradzp[i][j][k]);
                if(prewittgrad[i][j][k]>max)max=prewittgrad[i][j][k];
            }
        }
    }
    //normalization(prewittgrad);
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                if(a[i][j][k]!=0)
                prewittgrad[i][j][k]=max-prewittgrad[i][j][k];
            }
        }
    }
}

void normalization(unsigned int arr[sizex][sizey][sizez])
{
    double value;
    int i,j,k;
    unsigned int range=0;
    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                if(arr[i][j][k]>range)range=arr[i][j][k];
            }
        }
    }

    for(k=0;k<sizez;k++)
    {
        for(j=0;j<sizey;j++)
        {
            for(i=0;i<sizex;i++)
            {
                if(a[i][j][k]>0)
                {
                    value=arr[i][j][k]/(double)range;
                    value=round(value*USHRT_MAX);
                    focalness1[i][j][k]=(unsigned short)value;
                }
            }
        }
    }
}

